# Inventoro interview technical assignment

## Setup

##### 1. Clone and install dependencies:

```bash
$ git clone git@gitlab.com:luboy/inventoro-test.git
$ composer install
```

##### 2. To start up local dev environment run:

```bash
$ docker-compose up -d
```

This builds image from `Dockerfile` and sets up `Nginx` and `PHP` containers.

Application is exposing port `:8860`, if other docker container is using same port, please edit `docker-compose.yml` file.

## Endpoints

#### Product Detail

```bash
GET http://localhost:8860/product/{productId}
```

Product info is obtained from file cache as JSON, or from various DB repositories. 
In this example repositories use given drivers which are implemented as mock and only generates random price for products.

Different DB Repositories can be enabled in `config/services.yaml`
```bash
config/services.yaml

...
38     # App\Infrastructure\Repository\Interfaces\IProductRepository: '@App\Infrastructure\Repository\MySQLProductRepository'
39     App\Infrastructure\Repository\Interfaces\IProductRepository: '@App\Infrastructure\Repository\ElasticSearchProductRepository'
...
```

Same variability could be achieved for ICacheDriver implementation. As only one implementation exists, it's autowired automaticaly.



Response example for `http://localhost:8860/product/1`: 

```bash
{
    "id":"1",
    "name":"Product #1",
    "price":67.7
}
```

Every visit of the product detail page is counted. In this example the event is dispatched and handled via Symfony Messenger component. 
This is configured for Sync handling as this example lacks DB to dispatch asynchronously but is prepared for Async if needed. After configuring DB just switch 
the `SyncEventHandlerInterface` to `AsyncEventHandlerInterface` in `ProductDetailWasVisitedEventHandler` class.

#### Product Visits Stats

```bash
GET http://localhost:8860/stats
```

Visits Stats are saved in single JSON file as `"productId": count` pairs and the file name can be configured via .env:
```bash
41    STATS_FILENAME="product_visits_stats.json"
```

Response example: 

```bash
{
    "1":23
    "2":1,
}
```