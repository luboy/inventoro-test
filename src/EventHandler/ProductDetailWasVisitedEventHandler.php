<?php

namespace App\EventHandler;

use App\Event\ProductDetailWasVisitedEvent;
use App\EventHandler\Interfaces\SyncEventHandlerInterface;
use App\Facade\StatsFacade;

class ProductDetailWasVisitedEventHandler implements SyncEventHandlerInterface
{
    private StatsFacade $statsFacade;

    public function __construct(StatsFacade $statsFacade)
    {
        $this->statsFacade = $statsFacade;
    }

    public function __invoke(ProductDetailWasVisitedEvent $event): void
    {
        $this->statsFacade->increaseProductVisitsCount($event->getProductId());
    }
}