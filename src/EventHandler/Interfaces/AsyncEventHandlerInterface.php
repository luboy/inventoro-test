<?php declare(strict_types=1);

namespace App\EventHandler\Interfaces;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

interface AsyncEventHandlerInterface extends MessageHandlerInterface
{
}
