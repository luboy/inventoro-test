<?php


namespace App\Facade;


use App\Infrastructure\Repository\Interfaces\IStatsRepository;

class StatsFacade
{
    private IStatsRepository $statsRepository;

    public function __construct(IStatsRepository $statsRepository)
    {
        $this->statsRepository = $statsRepository;
    }

    public function getStats(): array
    {
        return $this->statsRepository->getAllStats();
    }

    public function increaseProductVisitsCount(string $productId): void
    {
        $productVisitCount = $this->statsRepository->increaseProductVisits($productId);

    }
}