<?php


namespace App\Facade;


use App\Infrastructure\Driver\Interfaces\ICacheDriver;
use App\Infrastructure\Repository\Interfaces\IProductRepository;

class ProductFacade
{
    private IProductRepository $productRepository;
    private ICacheDriver $cacheDriver;

    public function __construct(IProductRepository $productRepository, ICacheDriver $cacheDriver)
    {
        $this->productRepository = $productRepository;
        $this->cacheDriver = $cacheDriver;
    }

    public function getProductJson(string $productId): string
    {
        $product = $this->findInCache($productId);

        if ($product) {
            return $product;
        }

        $product = $this->productRepository->findById($productId);
        $productJson = json_encode($product);

        $this->cacheDriver->set($productId, $productJson);

        return $productJson;
    }

    private function findInCache(string $productId): ?string
    {
        return $this->cacheDriver->get($productId);
    }
}