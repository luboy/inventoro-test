<?php


namespace App\Controller;


use App\Facade\StatsFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatsController extends AbstractController
{
    /**
     * @Route("/stats", name="stats")
     * @return Response
     */
    public function stats(StatsFacade $statsFacade): Response
    {
        $stats = $statsFacade->getStats();

        return $this->json($stats);
    }
}