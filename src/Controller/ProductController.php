<?php

namespace App\Controller;

use App\Event\ProductDetailWasVisitedEvent;
use App\Facade\ProductFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/product/{productId}", name="product_detail")
     * @return Response
     */
    public function detail(string $productId, ProductFacade $productFacade, MessageBusInterface $messageBus): Response
    {
        $event = new ProductDetailWasVisitedEvent($productId);
        $messageBus->dispatch($event);

        $product = $productFacade->getProductJson($productId);

        return JsonResponse::fromJsonString($product, Response::HTTP_OK);
    }
}