<?php


namespace App\Entity;


class Product
{
    private string $id;
    private string $name;
    private float $price;

    public function __construct(string $id, string $name, float $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    public static function generate(string $id): self
    {
        $name = 'Product #'.$id;
        $price = rand(100, 1000) / 10;

        return new self($id, $name, $price);
    }

    public function getPrice(): float
    {
        return round($this->price,2);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->getPrice(),
        ];
    }
}