<?php


namespace App\Infrastructure\Driver;


use App\Infrastructure\Driver\Interfaces\ICacheDriver;

class FileCacheDriver implements ICacheDriver
{
    private string $cacheBaseDir;

    public function __construct(string $cacheDir)
    {
        $this->cacheBaseDir = $cacheDir;

    }

    public function set(string $index, string $value): void
    {
        $res = fopen($this->pathToFile($index), 'w+');
        fwrite($res, $value);
        fclose($res);
    }

    public function get(string $index): ?string
    {
        if (!file_exists($this->pathToFile($index))) {
            return null;
        }

        return file_get_contents($this->pathToFile($index));
    }

    private function pathToFile(string $index)
    {
        return $this->cacheBaseDir.'/file_'.$index;
    }
}