<?php


namespace App\Infrastructure\Driver\Interfaces;


interface IMySQLDriver
{
    public function findProduct(string $id): array;
}