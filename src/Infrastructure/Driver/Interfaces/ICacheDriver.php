<?php


namespace App\Infrastructure\Driver\Interfaces;


interface ICacheDriver
{
    public function set(string $index, string $value): void;
    public function get(string $index): ?string;
}