<?php


namespace App\Infrastructure\Driver\Interfaces;


interface IElasticSearchDriver
{
    public function findById(string $id): array;
}