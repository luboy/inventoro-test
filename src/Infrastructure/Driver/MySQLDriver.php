<?php


namespace App\Infrastructure\Driver;


use App\Entity\Product;
use App\Infrastructure\Driver\Interfaces\IMySQLDriver;

class MySQLDriver implements IMySQLDriver
{
    public function findProduct(string $id): array
    {
        return Product::generate($id)->toArray();
    }
}