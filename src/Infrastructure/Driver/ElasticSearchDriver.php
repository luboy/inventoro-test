<?php

namespace App\Infrastructure\Driver;

use App\Entity\Product;
use App\Infrastructure\Driver\Interfaces\IElasticSearchDriver;

class ElasticSearchDriver implements IElasticSearchDriver
{
    public function findById(string $id): array
    {
        return Product::generate($id)->toArray();
    }
}