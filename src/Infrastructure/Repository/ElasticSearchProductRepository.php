<?php


namespace App\Infrastructure\Repository;


use App\Infrastructure\Driver\Interfaces\IElasticSearchDriver;
use App\Infrastructure\Repository\Interfaces\IProductRepository;

class ElasticSearchProductRepository implements IProductRepository
{
    private IElasticSearchDriver $elasticSearchDriver;

    public function __construct(IElasticSearchDriver $elasticSearchDriver)
    {
        $this->elasticSearchDriver = $elasticSearchDriver;
    }

    public function findById(string $id): array
    {
        return $this->elasticSearchDriver->findById($id);
    }
}