<?php

namespace App\Infrastructure\Repository;

use App\Infrastructure\Driver\Interfaces\IMySQLDriver;
use App\Infrastructure\Repository\Interfaces\IProductRepository;

class MySQLProductRepository implements IProductRepository
{
    private IMySQLDriver $mySQLDriver;

    public function __construct(IMySQLDriver $mySQLDriver)
    {
        $this->mySQLDriver = $mySQLDriver;
    }

    public function findById(string $id): array
    {
        return $this->mySQLDriver->findProduct($id);
    }
}