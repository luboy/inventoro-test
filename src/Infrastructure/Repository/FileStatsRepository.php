<?php


namespace App\Infrastructure\Repository;


use App\Infrastructure\Repository\Interfaces\IStatsRepository;

class FileStatsRepository implements IStatsRepository
{
    private string $cacheDir;
    private string $statsFilename;

    public function __construct(string $cacheDir, string $statsFilename)
    {
        $this->cacheDir = $cacheDir;
        $this->statsFilename = $statsFilename;

        if( !file_exists($this->getPathToFile())) {
            $res = fopen($this->getPathToFile(), 'w+');
            fclose($res);
        }
    }

    public function getAllStats(): array
    {
        $statsJson = file_get_contents($this->getPathToFile());

        return $statsJson ? json_decode($statsJson, true) : [];
    }

    public function increaseProductVisits(string $productId)
    {
        $statsJson = file_get_contents($this->getPathToFile());
        $stats = json_decode($statsJson, true);

        if (is_null($stats)) {
            $stats = [];
        }

        if (!array_key_exists($productId, $stats)) {
            $stats[$productId] = 0;
        }

        $stats[$productId]++;

        $res = fopen($this->getPathToFile(), 'w+');
        fwrite($res, json_encode($stats));
        fclose($res);
    }

    private function getPathToFile(): string
    {
        return $this->cacheDir.'/'.$this->statsFilename;
    }
}