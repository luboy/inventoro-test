<?php


namespace App\Infrastructure\Repository\Interfaces;


interface IProductRepository
{
    public function findById(string $id): array;
}