<?php


namespace App\Infrastructure\Repository\Interfaces;


interface IStatsRepository
{
    public function increaseProductVisits(string $productId);

    public function getAllStats();
}