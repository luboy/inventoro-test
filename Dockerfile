ARG PHP_VERSION=7.4.6
FROM php:${PHP_VERSION}-fpm-alpine

LABEL maintainer="Vojtech Lubojacky<vojtech.lubojacky@gmail.com>"
EXPOSE 80
STOPSIGNAL SIGTERM
WORKDIR /var/www/html

COPY ./docker/php/php.ini /usr/local/etc/php/php.ini
COPY ./docker/php/php-cli.ini /usr/local/etc/php/php-cli.ini
COPY ./docker/php/docker-entrypoint.sh /usr/local/bin/docker-entrypoint

RUN apk add --no-cache \
    && chmod +x /usr/local/bin/docker-entrypoint

ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]